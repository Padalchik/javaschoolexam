package com.tsystems.javaschool.tasks.calculator;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement==null) {
            return null;
        }
        Character[] LegitArr = {'0','1','2','3','4','5','6','7','8','9','+' , '-' , '/' , '*' , '(' , ')' , '.' };
        Character[] Operations = {'+','-','/','*'};
        List<Character> LegitList = Arrays.asList(LegitArr);
        for (int i=0;i<statement.length();i++) {
            if(!LegitList.contains(statement.charAt(i))){
                return null;
            }
        }
        for (int i=1; i<statement.length()-1;i++) {
            for (int j=0;j<Operations.length;j++) {
                if(statement.charAt(i)==Operations[j]&&statement.charAt(i+1)==Operations[j])
                {
                    return null;
                }
            }
        }
        try {
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("js");

            Object preresult = engine.eval(statement).toString();
            if (preresult=="Infinity") {
                return null;
            }
            double result = Double.parseDouble(preresult.toString());
            NumberFormat nf = new DecimalFormat("#.####");
            return nf.format(result).replace(',','.');
        } catch(Exception ex){
            return null;
        }
    }
    
}
