package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        if (inputNumbers.size()>Integer.MAX_VALUE - 10) {
            throw new CannotBuildPyramidException();
        }
        Collections.sort(inputNumbers);
        for (int i = 0; i < inputNumbers.size() - 1; i++) {
            if (inputNumbers.get(i) == inputNumbers.get(i + 1)) {
                throw new CannotBuildPyramidException();
            }
        }
        int x = inputNumbers.size();
        int lev = 2;
        int vsego = 1;
        int NumbersInRow = 1;
        while (x > vsego) {
            vsego = vsego + lev;
            lev += 1;
            NumbersInRow += 2;
        }
        int row = lev - 1;
        if (x != vsego) {
            throw new CannotBuildPyramidException();
        }
        int[][] arr = new int[row][NumbersInRow];
        int space = row - 1;
        int current = 0;
        for (int i = 0; i < row; i++) {
            for (int j = space; j < NumbersInRow - space; j += 2) {
                arr[i][j] = inputNumbers.get(current);
                current += 1;
            }
            space -= 1;
        }
        return arr;
    }
    
}
